package com.getvymo.Spi.hierarchy_repositories;


import com.getvymo.data.entities.hierarchy.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Deepa on 24/04/17.
 */


@Repository
public interface UsersRepository extends JpaRepository<Users,String > {
    public List<Users> getByHierarchyKey(String hKeys);
    public List<Users> getByUserIdAndClientId(String userCode, String clientCode);


}
