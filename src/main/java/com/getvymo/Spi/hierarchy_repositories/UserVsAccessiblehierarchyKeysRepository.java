package com.getvymo.Spi.hierarchy_repositories;


import com.getvymo.data.entities.hierarchy.UserVsAccessibleHierarchyKeys;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Deepa on 24/04/17.
 */


@Repository
public interface UserVsAccessiblehierarchyKeysRepository extends CrudRepository<UserVsAccessibleHierarchyKeys,Long> {
    public List<UserVsAccessibleHierarchyKeys> findByUserId(String userId);
    public List<UserVsAccessibleHierarchyKeys> findByUserIdAndClientId(String userId, String clientId);
}
