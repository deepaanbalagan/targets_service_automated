package com.getvymo.Spi.target_repositories;


import com.getvymo.data.entities.target_service.TargetDefinition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Created by deepaanbalagan on 18/04/17.
 */

@Repository
public interface TargetDefinitionRepository extends JpaRepository<TargetDefinition, String> {
    public TargetDefinition findByFactId(String factId);
    public TargetDefinition findByFactIdAndClientCodeAndGranularity(String factId, String clientCode, String ganularity);
}
