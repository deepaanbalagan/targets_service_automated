package com.getvymo.Spi.target_repositories;

import com.getvymo.data.entities.target_service.FactMetrics;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

/**
 * Created by deepaanbalagan on 24/04/17.
 */
public interface MetricFactRepository extends JpaRepository<FactMetrics,Integer> {
    public FactMetrics findByFactIdAndValue(String factId, String metricValue);
    public FactMetrics findByFactIdAndFactDimension(String factId, String metricValue);
    public ArrayList<FactMetrics> findById(int id);

}
