package com.getvymo.Spi.target_repositories;


import com.getvymo.data.entities.target_service.TargetInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by deepaanbalagan on 18/04/17.
 */
@Repository
public interface TargetInstanceRepository extends JpaRepository<TargetInstance,String> {
    public TargetInstance findById(int id);

    public TargetInstance findByClientCodeAndMetricFactIdAndAssignmentTypeAndDateKeyAndAchievementType(String
                                                                                                               clientCode, int metricFactId, String assignementType, String dateKey, String achievementType);
}
