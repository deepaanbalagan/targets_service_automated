package com.getvymo.Spi.target_repositories;

import com.getvymo.data.entities.target_service.DateKeys;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
/**
 * Created by deepaanbalagan on 18/04/17.
 */
@Repository
public interface TargetRepositoryDate extends JpaRepository<DateKeys, Integer> {

	public List<DateKeys> findAll();
	public List<DateKeys> findAllByOrderByDateKeysPKAsc();
	
}
