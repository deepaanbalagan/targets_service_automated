package com.getvymo.Spi.target_repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.getvymo.data.entities.target_service.Facts;

import java.util.List;

/**
 * Created by deepaanbalagan on 24/04/17.
 */
public interface FactsRepository extends JpaRepository<Facts,Integer> {

  public List<Facts> findByClientCode(String clientCode);
}
