package com.getvymo.Spi.target_repositories;


import com.getvymo.data.entities.target_service.UserVsTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.List;
/**
 * Created by deepaanbalagan on 18/04/17.
 */

@Component
public interface TargetRespository extends JpaRepository<UserVsTarget, Long> {

	public List<UserVsTarget> findByUserCode(String userCode);

	public List<UserVsTarget> findByHierarchyKey(String list);

	UserVsTarget findByUserCodeAndInstanceId(String userCode, int instanceId);



}
