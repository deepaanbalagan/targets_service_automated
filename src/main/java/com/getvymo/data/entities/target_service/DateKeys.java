package com.getvymo.data.entities.target_service;

/**
 * Created by deepaanbalagan on 20/04/17.
 */

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Date;

@Entity
@Table(name = "date_keys", schema = "targets_service")
public class DateKeys {
    @EmbeddedId
    private DateKeysPK dateKeysPK;
    @Column(name = "day_key")
    private String dayKey;
    @Column(name = "week_key")
    private String weekKey;
    @Column(name = "month_key")
    private String monthKey;
    @Column(name = "year_key")
    private String yearKey;
    @Column(name = "quarter_key")
    private String quarterKey;


    public DateKeys() {
    }



    public DateKeys(Date id, String dayKey, String weekKey, String monthKey, String yearKey, String quarterKey,
                    String clientCode) {
        this.dayKey = dayKey;
        this.weekKey = weekKey;

        this.monthKey = monthKey;
        this.yearKey = yearKey;
        this.quarterKey = quarterKey;
    }
    public DateKeysPK getDateKeysPK() {
        return dateKeysPK;
    }

    public void setDateKeysPK(DateKeysPK dateKeysPK) {
        this.dateKeysPK = dateKeysPK;
    }


    public String getDayKey() {
        return dayKey;
    }

    public void setDayKey(String dayKey) {
        this.dayKey = dayKey;
    }

    public String getWeekKey() {
        return weekKey;
    }

    public void setWeekKey(String weekKey) {
        this.weekKey = weekKey;
    }

    public String getMonthKey() {
        return monthKey;
    }

    public void setMonthKey(String monthKey) {
        this.monthKey = monthKey;
    }

    public String getYearKey() {
        return yearKey;
    }

    public void setYearKey(String yearKey) {
        this.yearKey = yearKey;
    }

    public String getQuarterKey() {
        return quarterKey;
    }

    public void setQuarterKey(String quarterKey) {
        this.quarterKey = quarterKey;
    }




    public String getKey(String period) {
        if (period.equals("week")) {
            return getWeekKey();

        } else if (period.equals("month")) {
            return getMonthKey();
        } else if (period.equals("quarter")) {
            return getQuarterKey();
        } else  if (period.equals("year")){
            return getYearKey();
        } else {
            return getDayKey();
        }
    }

}
