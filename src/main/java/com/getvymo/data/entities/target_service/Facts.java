package com.getvymo.data.entities.target_service;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "facts", schema = "targets_service")
@IdClass(Facts.class)
public class Facts implements Serializable {
    static final long serialVersionUID = 42L;
    @Column(name="id")
    private String id;
    @Id
    @Column(name="metric")
    private String metric;
    @Id
    @Column(name="module")
    private String module;
    @Id
    @Column(name = "client_code")
    private String clientCode;
    @Id
    @Column(name = "state")
    private String state;
    @Column(name = "dimensionId")
    @Id
    private String dimensionId;



    public Facts() {
    }

//    public Facts(String id, String metric, String module, String clientCode, String state,
//                 String dimensionId) {
//        this.id = id;
//        this.metric = metric;
//        this.module = module;
//        this.clientCode = clientCode;
//        this.state = state;
//        this.dimensionId = dimensionId;
//    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(String dimensionId) {
        this.dimensionId = dimensionId;
    }
}
