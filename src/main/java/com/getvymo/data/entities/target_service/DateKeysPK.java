package com.getvymo.data.entities.target_service;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.sql.Date;

@Embeddable
public class DateKeysPK implements Serializable {

    @Column(name = "id", nullable = false)
    private Date id;

    @Column(name = "client_code")
    private String clientCode;

    public Date getId() {
        return id;
    }

    public void setId(Date id) {
        this.id = id;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }




}
