package com.getvymo.data.entities.target_service;

/**
 * Created by deepaanbalagan on 20/04/17.
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "target_definition", schema = "targets_service")
public class TargetDefinition {
    @Id
//	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String granularity;
    @Column(name = "fact_id")
    private String factId;
    @Column(name = "client_code")
    private String clientCode;

    public TargetDefinition() {
    }

    public TargetDefinition(int id, String granularity, String factId, String clientCode) {
        this.granularity = granularity;
        this.factId = factId;
        this.clientCode = clientCode;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGranularity() {
        return granularity;
    }

    public void setGranularity(String granularity) {
        this.granularity = granularity;
    }

    public String getFactId() {
        return factId;
    }

    public void setFactId(String factId) {
        this.factId = factId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    @Override
    public String toString() {
        return "TargetDefinition{" +
                "id=" + id +
                ", granularity='" + granularity + '\'' +
                ", factId='" + factId + '\'' +
                ", clientCode='" + clientCode + '\'' +
                '}';
    }
}

