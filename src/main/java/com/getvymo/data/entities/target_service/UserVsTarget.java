package com.getvymo.data.entities.target_service;

/**
 * Created by deepaanbalagan on 20/04/17.
 */

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_vs_target", schema = "targets_service")
@IdClass(UserVsTarget.class)
public class UserVsTarget implements Serializable {

    static final long serialVersionUID = 42L;
    @Id
    @Column(name = "user_code")
    private String userCode;
    @Id
    @Column(name = "hierarchy_key")
    private String hierarchyKey;
    @Id
    @Column(name = "instance_id")
    private Integer instanceId;
    @Id
    @Column(name = "metric_fact_id")
    private int metricFactId;
    @Column(name = "date_key")
    private String dateKey;
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "value")
    private int value;

    public UserVsTarget() {
    }

    public UserVsTarget(String userCode, String hierarchyKey, int instanceId, int metricFactId, String dateKey,
                        String clientCode, int value) {
        this.userCode = userCode;
        this.hierarchyKey = hierarchyKey;
        this.instanceId = instanceId;
        this.metricFactId = metricFactId;
        this.dateKey = dateKey;
        this.clientCode = clientCode;
        this.value = value;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getHierarchyKey() {
        return hierarchyKey;
    }

    public void setHierarchyKey(String hierarchyKey) {
        this.hierarchyKey = hierarchyKey;
    }

    public Integer getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    public int getMetricFactId() {
        return metricFactId;
    }

    public void setMetricFactId(int metricFactId) {
        this.metricFactId = metricFactId;
    }

    public String getDateKey() {
        return dateKey;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) { return true; }
        if (obj == null || obj.getClass() != this.getClass()) { return false; }
        UserVsTarget userVsTarget = (UserVsTarget) obj;


        return ((this.userCode.equalsIgnoreCase(userVsTarget.userCode) && (this.dateKey.equalsIgnoreCase(userVsTarget.dateKey) )&&
                (this.hierarchyKey.equalsIgnoreCase(userVsTarget.hierarchyKey) )&&(this.instanceId ==(userVsTarget.instanceId))
                &&(this.value==userVsTarget.value)&&(this.clientCode.equalsIgnoreCase(userVsTarget.clientCode))));


    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
