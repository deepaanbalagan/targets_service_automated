package com.getvymo.data.entities.target_service;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "metric_fact" , schema = "targets_service")
@IdClass(FactMetrics.class)
public class FactMetrics implements Serializable{
    static final long serialVersionUID = 42L;
    @Column(name="id")
    private int id;
    @Id
    @Column(name = "fact_id")
    private String factId;
    @Id
    @Column(name = "fact_dimension")
    private String factDimension;
    @Id
    @Column(name="value")
    private String value;
    @Id
    @Column(name = "client_code")
    private String clientCode;
    @Column(name = "unique_key")
    private String uniqueKey;


    public FactMetrics() {

    }

//    public FactMetrics(String factId, String factDimension, String value, String clientCode, String uniqueKey) {
//        this.factId = factId;
//        this.factDimension = factDimension;
//        this.value = value;
//        this.clientCode = clientCode;
//        this.uniqueKey = uniqueKey;
//    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFactId() {
        return factId;
    }

    public void setFactId(String factId) {
        this.factId = factId;
    }

    public String getFactDimension() {
        return factDimension;
    }

    public void setFactDimension(String factDimension) {
        this.factDimension = factDimension;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }
}
