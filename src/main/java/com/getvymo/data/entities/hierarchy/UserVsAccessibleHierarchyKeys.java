package com.getvymo.data.entities.hierarchy;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Deepa on 23/04/17.
 */


@Entity
@Table(name = "usersVsAccessibleHierarchyKeys", schema = "org_hierarchy_service")
@IdClass(UserVsAccessibleHierarchyKeys.class)
public class UserVsAccessibleHierarchyKeys  implements Serializable{
    @Id
    private String userId;
    @Id
    private String clientId;
    @Id
    private String hierarchyKey;
    private int own;

    public UserVsAccessibleHierarchyKeys() {
    }

    public UserVsAccessibleHierarchyKeys(String userId, String clientId, String hierarchyKey, int own) {
        this.userId = userId;
        this.clientId = clientId;
        this.hierarchyKey = hierarchyKey;
        this.own = own;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getHierarchyKey() {
        return hierarchyKey;
    }

    public void setHierarchyKey(String hierarchyKey) {
        this.hierarchyKey = hierarchyKey;
    }

    public int getOwn() {
        return own;
    }

    public void setOwn(int own) {
        this.own = own;
    }

    @Override
    public String toString() {
        return "UserVsAccessibleHierarchyKeys{" +
                "userId='" + userId + '\'' +
                ", clientId='" + clientId + '\'' +
                ", hierarchyKey='" + hierarchyKey + '\'' +
                ", own=" + own +
                '}';
    }
}
