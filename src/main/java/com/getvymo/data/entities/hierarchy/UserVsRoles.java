package com.getvymo.data.entities.hierarchy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Deepa on 23/04/17.
 */
@Entity
@Table(name = "userVsRoles", schema = "org_hierarchy_service")
@IdClass(UserVsRoles.class)
public class UserVsRoles implements Serializable{
    @Id
    @Column(name = "userId")
    private String userCode;
    @Id
    @Column(name = "hierarchyKey")
    private String hierarchyKey;
    @Id
    @Column(name = "clientId")
    private String clientCode;
    @Id
    @Column(name = "role")
    private String role;

    public UserVsRoles(String userCode, String hierarchyKey, String clientCode, String role) {
        this.userCode = userCode;
        this.hierarchyKey = hierarchyKey;
        this.clientCode = clientCode;
        this.role = role;
    }

    public UserVsRoles(String userCode) {
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getHierarchyKey() {
        return hierarchyKey;
    }

    public void setHierarchyKey(String hierarchyKey) {
        this.hierarchyKey = hierarchyKey;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
