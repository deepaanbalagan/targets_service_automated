package com.getvymo.data.entities.hierarchy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Deepa on 23/04/17.
 */

@Entity
@Table(name = "users", schema = "org_hierarchy_service")
public class Users {
    @Id
    private String userId;
    private String clientId;
    private String hierarchyKey;
    private String name;
    private String email;
    private String phone;
    @Column(columnDefinition = "TEXT")
    private String params;
    private int isLatestKey;
    private int disabled;
    private String managerId;
    private java.util.Date disabledOn;
    private java.util.Date updatedTime;
    private String regionType;

    public Users() {
    }

    public Users(String userId, String clientId, String hierarchyKey, String name, String email, String phone,
                 String params, int isLatestKey, int disabled, String managerId, java.util.Date disabledOn,
                 java.util.Date updatedTime, String regionType) {
        this.userId = userId;
        this.clientId = clientId;
        this.hierarchyKey = hierarchyKey;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.params = params;
        this.isLatestKey = isLatestKey;
        this.disabled = disabled;
        this.managerId = managerId;
        this.disabledOn = disabledOn;
        this.updatedTime = updatedTime;
        this.regionType = regionType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getHierarchyKey() {
        return hierarchyKey;
    }

    public void setHierarchyKey(String hierarchyKey) {
        this.hierarchyKey = hierarchyKey;
    }

    public int getDisabled() {
        return disabled;
    }

    public void setDisabled(int disabled) {
        this.disabled = disabled;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public java.util.Date getDisabledOn() {
        return disabledOn;
    }

    public void setDisabledOn(java.util.Date disabledOn) {
        this.disabledOn = disabledOn;
    }
}
