package com.getvymo.response.entities;

import java.io.Serializable;
import java.util.HashMap;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;
import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * Created by deepaanbalagan on 18/04/17.
 */
public class ActiveTarget implements Serializable{

    private static final long serialVersionUID = 1L;


    @JsonProperty("name")
    String name;
    @JsonProperty("instance_id")
    Integer instanceId;
    @JsonProperty("fact_definition_id")
    int factDefId;
    @JsonProperty("metric_id")
    String metricId;
    @JsonProperty("metric_fact_id")
    Integer metricFactId;
    // @JsonProperty("metric_value")
    // MetricFacts metricFacts;
    @JsonProperty("filters")
    HashMap<String, String> metricFact;

    @JsonProperty("target_value")
    Integer value;
    @JsonProperty("target_type")
    String targetType;

    @JsonProperty("start_date")
    long startDate;
    @JsonProperty("end_date")
    long endDate;

    @JsonProperty("description")
    String description;
    @JsonProperty("active")
    boolean active;
    @JsonProperty("achieved")
    Float achieved;
    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("user_id")
    private String userId;
    @JsonIgnore
    private String metricValue;
    @JsonIgnore
    private String factDimension;
    @JsonIgnore
    private String dateKey;
//    private String metricValue;
//
//    private String factDimension;
//
//    private String dateKey;

//    public String getMetricValue() {
//        return metricValue;
//    }
//
//    public void setMetricValue(String metricValue) {
//        this.metricValue = metricValue;
//    }
//
//    public String getFactDimension() {
//        return factDimension;
//    }
//
//    public void setFactDimension(String factDimension) {
//        this.factDimension = factDimension;
//    }
//
//    public String getDateKey() {
//        return dateKey;
//    }
//
//    public void setDateKey(String dateKey) {
//        this.dateKey = dateKey;
//    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public HashMap<String, String> getMetricFact() {
        return metricFact;
    }

    public void setMetricFact(HashMap<String, String> metricFact) {
        this.metricFact = metricFact;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAchieved() {
        return achieved;
    }

    public void setAchieved(float achieved) {
        this.achieved = achieved;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Integer instanceId) {
        this.instanceId = instanceId;
    }

    public int getFactDefId() {
        return factDefId;
    }

    public void setFactDefId(int factDefId) {
        this.factDefId = factDefId;
    }

    public String getMetricId() {
        return metricId;
    }

    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }

    public Integer getMetricFactId() {
        return metricFactId;
    }

    public void setMetricFactId(Integer metricFactId) {
        this.metricFactId = metricFactId;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long l) {
        this.startDate = l;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }



}
