package com;

import com.getvymo.Spi.hierarchy_repositories.UserVsAccessiblehierarchyKeysRepository;
import com.getvymo.Spi.target_repositories.*;
import com.getvymo.data.entities.hierarchy.UserVsAccessibleHierarchyKeys;
import com.getvymo.data.entities.target_service.*;
import com.getvymo.response.entities.ActiveTarget;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.util.*;

import static com.jayway.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest

public class UserDetailsTest {


    @Autowired
    TargetDefinitionRepository definitionRepository;
    @Autowired
    TargetInstanceRepository targetInstanceRepository;
    @Autowired
    TargetRepositoryDate targetRepositoryDate;
    @Autowired
    TargetRespository uservstargetRepository;
    @Autowired
    UserVsAccessiblehierarchyKeysRepository userVsAccessibleHierarchyKeysRepo;
    @Autowired
    MetricFactRepository metricFactRepository;
    @Autowired
    FactsRepository factsRepository;

    public static Response response;
    private static String jsonAsString;
    // user whom test cases should be executed
    static final String userId = "test_user";
    static final String userIdSingle="test_user_one";
    static final String clientId = "indiafirstlife";
    static final Integer targetdefinitionid = 20;
    static final Integer metricfactid = 100;
    static final String startDate = "2017-04-01";
    static final String endDate = "2017-04-27";
    static final String baseURL = "http://localhost:8080/targets/v1/user/{userid}";
//    String startDate="12-31-2014";


    //    List<Integer> targetDefintionIds = new ArrayList<Integer>();
    public static List<UserVsTarget> userVsTargets = new ArrayList<UserVsTarget>();
    public  static List<TargetDefinition> targetDefinitions = new ArrayList<TargetDefinition>();
    TargetInstance targetSingleInstance;
    UserVsAccessibleHierarchyKeys userVsAccessibleHierarchyKeys;
//    List<DateKeys> dateKeys = new ArrayList<DateKeys>();
    public static List<UserVsAccessibleHierarchyKeys> userVsAccessibleHierarchyKeysList=new ArrayList<UserVsAccessibleHierarchyKeys>();
    public static HashMap<Integer, TargetDefinition> targetDefinitionHashMap = new HashMap<Integer, TargetDefinition>();
    public static HashMap<Integer, TargetInstance> targetInstance = new HashMap<Integer, TargetInstance>();
    public static List<Facts> factsList=new ArrayList<Facts>();
    public static List<FactMetrics> factMetricsList=new ArrayList<FactMetrics>();


    @Test
    public void contextLoads() {
    }

    @Test
    public void runMultipleInstancesAll()
    {
        insertUsersVsAccessibleHierarchyKeys();
        insertDateKeys();
        insertFacts();
        insertMetricFact();
        insertTargetDefinition();
        insertTargetInstance();
        insertUserVsTarget();
        responseGetAllActiveTargetsTestSelfFalseResponseInstancesVerify();
        responseVerificationOfMultipleInsatncesSelfTrue();
        responseVerificationOfMultipleDefinitionsSelfTrue();
        responseVerificationOfMultipleDefinitionsSelfFalse();
        responseGetAllActiveTargetsTestSelfTrueResponseInstancesVerifyTrue();
        responseGetAllActiveTargetsTestSelfTrueResponseInstancesVerifyFalse();
        responseVerificationDateFilterOfMultipleInsatncesSelfTrue();
        responseVerificationDateFilterOfMultipleInsatncesSelfFalse();
        responseDateFilterDefinitionsWhenSelfTrue();
        responseDateFilterDefinitionsWhenSelfFalse();
        responseDateFilterDefFilterDetailedViewInstancesWhenSelfTrue();
        responseDefFilterDetailedViewInstancesWhenSelfFalse();
    }
    // test case for single Target Instance
    @Test
    public void runSingleInstanceAll()
    {
        insertDateKeys();
        inserUserVsAccessibleHierarchyKeysForSingleInstance();
        insertUserVsTargetForSingleInstance();
        insertSingleInstance();
        validateResponseSingleInstanceTrue();
        validateResponseSingleInstanceFalse();

    }
    // insert User Vs Accessible Hierarchy Keys into database

    public void insertUsersVsAccessibleHierarchyKeys() {
        UserVsAccessibleHierarchyKeys userVsAccessibleHierarchyKeys=new UserVsAccessibleHierarchyKeys();
        userVsAccessibleHierarchyKeys.setClientId(clientId);
        userVsAccessibleHierarchyKeys.setUserId(userId);
        userVsAccessibleHierarchyKeys.setHierarchyKey("1");
        userVsAccessibleHierarchyKeys.setOwn(1);
        UserVsAccessibleHierarchyKeys newUserVsAccHierarchyKeys =userVsAccessibleHierarchyKeysRepo .save(userVsAccessibleHierarchyKeys);
        userVsAccessibleHierarchyKeysList.add(newUserVsAccHierarchyKeys);

    }
    // insert date keys into database

    public void insertDateKeys() {
        Date date = new Date(System.currentTimeMillis());
        DateKeys dateKeys = new DateKeys();
        DateKeysPK pk = new DateKeysPK();
        pk.setId(date);
        pk.setClientCode(clientId);
        dateKeys.setDateKeysPK(pk);
        dateKeys.setDayKey("d-130-17_test");
        dateKeys.setWeekKey("w-100-17_test");
        dateKeys.setMonthKey("m-04-17_test");
        dateKeys.setYearKey("y-17_test");
        dateKeys.setQuarterKey("Weekly_test");
        DateKeys newDateKey = targetRepositoryDate.save(dateKeys);
        System.out.println("UVT SAVED" + newDateKey.toString());
    }
    // insert Fact into data base

    public void insertFacts() {
        Facts facts=new Facts();
        facts.setId("lead_creation_test");
        facts.setModule("activities");
        facts.setMetric("lead_creation_test_count");
        facts.setState("progress");
        facts.setClientCode(clientId);
        facts.setDimensionId("1");
        Facts newFact = factsRepository.save(facts);
        factsList.add(newFact);
        Facts factsTwo=new Facts();
        factsTwo.setId("premium_collected_test");
        factsTwo.setModule("activities");
        factsTwo.setMetric("premium_collected_test_count");
        factsTwo.setState("progress");
        factsTwo.setClientCode(clientId);
        factsTwo.setDimensionId("1");
        Facts newFactTwo = factsRepository.save(factsTwo);
        factsList.add(newFactTwo);
    }

    // insert metric facts into data base

    public void insertMetricFact() {
        FactMetrics metrics=new FactMetrics();
        metrics.setId(100);
        metrics.setFactId("lead_creation_test");
        metrics.setFactDimension("activity");
        metrics.setValue("lead_creation_test_count");
        metrics.setClientCode(clientId);
        metrics.setUniqueKey("1_lead_creations_test_key");
        FactMetrics newFactMetrics = metricFactRepository.save(metrics);
        factMetricsList.add(newFactMetrics);
        FactMetrics metricsTwo=new FactMetrics();
        metricsTwo.setId(200);
        metricsTwo.setFactId("premium_collected_test");
        metricsTwo.setFactDimension("activity");
        metricsTwo.setValue("premium_collected_test_count");
        metricsTwo.setClientCode(clientId);
        metricsTwo.setUniqueKey("premium_collected_test_key");
        FactMetrics newFactMetricsTwo = metricFactRepository.save(metricsTwo);
        factMetricsList.add(newFactMetricsTwo);

    }
    // insert target definition into data base

    public void insertTargetDefinition() {
        System.out.println("inside def method test");
        TargetDefinition definition = new TargetDefinition();
        definition.setId(20);
        definition.setFactId("lead_creation_test");
        definition.setClientCode(clientId);
        definition.setGranularity("weekly_test");

        targetDefinitionHashMap.put(20, definition);
        targetDefinitions.add(definition);

        TargetDefinition defOne=definitionRepository.save(definition);
        System.out.println("def one"+defOne.getFactId());
        TargetDefinition definitionTwo = new TargetDefinition();
        definitionTwo.setId(30);
        definitionTwo.setFactId("premium_collected_test");
        definitionTwo.setClientCode(clientId);
        definitionTwo.setGranularity("weekly_test");
        targetDefinitionHashMap.put(30, definitionTwo);
        targetDefinitions.add(definitionTwo);

       TargetDefinition defTwo= definitionRepository.save(definitionTwo);

    }
    // insert target instance into data base

    public void insertTargetInstance() {

        TargetInstance instance = new TargetInstance();
        instance.setId(12);
        instance.setDefId(20);
        instance.setMetricFactId(100);
        instance.setLabel("lead_creation_test");
        instance.setDescription("testing_of_lead_creation_test_target");
        instance.setDateKey("w-100-17_test");
        instance.setClientCode(clientId);
        instance.setAssignementType("team_test");
        instance.setAchievementType("self_test");
        targetInstanceRepository.save(instance);
        targetInstance.put(12, instance);
        TargetInstance instanceTwo = new TargetInstance();
        instanceTwo.setId(13);
        instanceTwo.setDefId(30);
        instanceTwo.setMetricFactId(200);
        instanceTwo.setLabel("premium_collect_fact_test");
        instanceTwo.setDescription("testing_of_premium_collect_fact_test");
        instanceTwo.setDateKey("w-100-17_test");
        instanceTwo.setClientCode(clientId);
        instanceTwo.setAssignementType("individual_test");
        instanceTwo.setAchievementType("team_test");
        targetInstance.put(13, instanceTwo);
        targetInstanceRepository.save(instanceTwo);
        TargetInstance instanceThree = new TargetInstance();
        instanceThree.setId(14);
        instanceThree.setDefId(20);
        instanceThree.setMetricFactId(100);
        instanceThree.setLabel("lead_creation_fact_test_monthly");
        instanceThree.setDescription("lead_creation_fact_test_monthly_def");
        instanceThree.setDateKey("m-04-17_test");
        instanceThree.setClientCode(clientId);
        instanceThree.setAssignementType("all_hierarchy_test");
        instanceThree.setAchievementType("team_test");
        targetInstance.put(14, instanceThree);
        targetInstanceRepository.save(instanceThree);
        TargetInstance instanceFour = new TargetInstance();
        instanceFour.setId(15);
        instanceFour.setDefId(20);
        instanceFour.setMetricFactId(100);
        instanceFour.setLabel("lead_creation_fact_test_weekly");
        instanceFour.setDescription("lead_creation_fact_test_weekly_def");
        instanceFour.setDateKey("w-100-17_test");
        instanceFour.setClientCode(clientId);
        instanceFour.setAssignementType("all_hierarchy");
        instanceFour.setAchievementType("self_test");
        targetInstance.put(15, instanceFour);
        targetInstanceRepository.save(instanceFour);
        TargetInstance instanceFive = new TargetInstance();
        instanceFive.setId(16);
        instanceFive.setDefId(30);
        instanceFive.setMetricFactId(200);
        instanceFive.setLabel("premium_collect_fact_test_monthly");
        instanceFive.setDescription("premium_collect_fact_test_monthly_def");
        instanceFive.setDateKey("m-04-17_test");
        instanceFive.setClientCode(clientId);
        instanceFive.setAssignementType("individual_test");
        instanceFive.setAchievementType("self_test");
        targetInstance.put(16, instanceFive);
        targetInstanceRepository.save(instanceFive);


    }
    // single instance insertion
    public void insertSingleInstance()
    {


        TargetInstance singleInstance = new TargetInstance();
        singleInstance.setId(21);
        singleInstance.setDefId(20);
        singleInstance.setMetricFactId(100);
        singleInstance.setLabel("lead_creation_fact_test_monthly");
        singleInstance.setDescription("lead_creation_fact_test_monthly_def");
        singleInstance.setDateKey("m-04-17_test");
        singleInstance.setClientCode(clientId);
        singleInstance.setAssignementType("individual_test");
        singleInstance.setAchievementType("self_test");
        targetInstanceRepository.save(singleInstance);
        targetSingleInstance=singleInstance;

    }
    //user insertion for single instance test
    public void insertUserVsTargetForSingleInstance()
    {

        UserVsTarget singleUserVsTarget = new UserVsTarget();
        singleUserVsTarget.setUserCode("test_user_single");
        singleUserVsTarget.setDateKey("m-04-17_test");
        singleUserVsTarget.setHierarchyKey("2");
        singleUserVsTarget.setInstanceId(21);
        singleUserVsTarget.setMetricFactId(100);
        singleUserVsTarget.setClientCode(clientId);
        singleUserVsTarget.setValue(450);
        uservstargetRepository.save(singleUserVsTarget);
    }
    // user vs accessible hierarchy keys insertion for single instance test
    public void inserUserVsAccessibleHierarchyKeysForSingleInstance()
    {
        UserVsAccessibleHierarchyKeys userVsAccessibleHierarchyKeysTwo=new UserVsAccessibleHierarchyKeys();
        userVsAccessibleHierarchyKeysTwo.setClientId(clientId);
        userVsAccessibleHierarchyKeysTwo.setUserId(userIdSingle);
        userVsAccessibleHierarchyKeysTwo.setHierarchyKey("2");
        userVsAccessibleHierarchyKeysTwo.setOwn(0);
        userVsAccessibleHierarchyKeysRepo .save(userVsAccessibleHierarchyKeysTwo);
        userVsAccessibleHierarchyKeys=userVsAccessibleHierarchyKeysTwo;
    }
    // insert user vs target into data base

    public void insertUserVsTarget() {

        UserVsTarget userVsTarget = new UserVsTarget();
        userVsTarget.setUserCode("test_user_one");
        userVsTarget.setDateKey("m-04-17_test");
        userVsTarget.setHierarchyKey("1");
        userVsTarget.setInstanceId(12);
        userVsTarget.setMetricFactId(100);
        userVsTarget.setClientCode(clientId);
        userVsTarget.setValue(1100);
        UserVsTarget newUserVsTarget = uservstargetRepository.save(userVsTarget);
        userVsTargets.add(newUserVsTarget);
        UserVsTarget userVsTargetNew = new UserVsTarget();
        userVsTargetNew.setUserCode("test_user_merge");
        userVsTargetNew.setDateKey("m-04-17_test");
        userVsTargetNew.setHierarchyKey("1");
        userVsTargetNew.setInstanceId(12);
        userVsTargetNew.setMetricFactId(100);
        userVsTargetNew.setClientCode(clientId);
        userVsTargetNew.setValue(700);
        UserVsTarget userVsTargetUpdated = uservstargetRepository.save(userVsTargetNew);
        userVsTargets.add(userVsTargetUpdated);
        UserVsTarget userVsTargetTwo = new UserVsTarget();
        userVsTargetTwo.setUserCode("test_user_merge");
        userVsTargetTwo.setDateKey("w-100-17_test");
        userVsTargetTwo.setHierarchyKey("1");
        userVsTargetTwo.setInstanceId(13);
        userVsTargetTwo.setMetricFactId(200);
        userVsTargetTwo.setClientCode(clientId);
        userVsTargetTwo.setValue(50);
        UserVsTarget userVsTargetUpdatedTwo = uservstargetRepository.save(userVsTargetTwo);
        userVsTargets.add(userVsTargetUpdatedTwo);
        UserVsTarget userVsTargetThree = new UserVsTarget();
        userVsTargetThree.setUserCode("test_user_three");
        userVsTargetThree.setDateKey("m-04-17_test");
        userVsTargetThree.setHierarchyKey("1");
        userVsTargetThree.setInstanceId(14);
        userVsTargetThree.setMetricFactId(100);
        userVsTargetThree.setClientCode(clientId);
        userVsTargetThree.setValue(950);
        UserVsTarget userVsTargetUpdatedTree = uservstargetRepository.save(userVsTargetThree);
        userVsTargets.add(userVsTargetUpdatedTree);
        UserVsTarget userVsTargetFour = new UserVsTarget();
        userVsTargetFour.setUserCode("test_user_four");
        userVsTargetFour.setDateKey("w-100-17_test");
        userVsTargetFour.setHierarchyKey("1");
        userVsTargetFour.setInstanceId(15);
        userVsTargetFour.setMetricFactId(100);
        userVsTargetFour.setClientCode(clientId);
        userVsTargetFour.setValue(80);
        UserVsTarget userVsTargetUpdatedFour = uservstargetRepository.save(userVsTargetFour);
        userVsTargets.add(userVsTargetUpdatedFour);
        UserVsTarget userVsTargetFive = new UserVsTarget();
        userVsTargetFive.setUserCode("test_user_four");
        userVsTargetFive.setDateKey("m-04-17_test");
        userVsTargetFive.setHierarchyKey("1");
        userVsTargetFive.setInstanceId(16);
        userVsTargetFive.setMetricFactId(200);
        userVsTargetFive.setClientCode(clientId);
        userVsTargetFive.setValue(900);
        UserVsTarget userVsTargetUpdatedFive = uservstargetRepository.save(userVsTargetFive);
        userVsTargets.add(userVsTargetUpdatedFive);

    }

    // validate the JSON response
    @Test
    public void validateResponseFalse() {
        given().param("self", false).
                param("clientcode", clientId).get(baseURL, userIdSingle).then().body(matchesJsonSchemaInClasspath("response.json"));

    }
    // validate the JSON response self = true
    @Test
    public void validateResponseTrue() {
        given().param("self", true).
                param("clientcode", clientId).get(baseURL, userIdSingle).then().body(matchesJsonSchemaInClasspath("emptyresponse.json"));

    }


    // Check the REST end point for getting all active targets when self= true - one target instance
    @Test
    public void uriStatusForGetAllActiveTargetsTestSelfTrue() {
        given().param("self", true).
                param("clientcode", clientId).get(baseURL, userId).then().statusCode(200).log().all();
    }


    // Check the REST end point for getting all active targets when self= false - one target instance
    @Test
    public void uriStatusForGetAllActiveTargetsTestSelfFalse() {
        given().param("self", false).
                param("clientcode", clientId).get(baseURL, userId).then().statusCode(200).log().all();
    }
    // single instance self = true

    public void validateResponseSingleInstanceTrue()
    {
        JsonPath jsonPath = given().param("self", true)
                .param("clientcode", clientId)
                .get(baseURL, userIdSingle).then().contentType(ContentType.JSON).extract().body().jsonPath();
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        if(activeTargets.size()==0) {

                    assertTrue(true);
        }
        else
            fail("should not return any instances, user own is set to 0");


    }
    // single instance self = false

    public void validateResponseSingleInstanceFalse()
    {
        JsonPath jsonPath = given().param("self", false)
                .param("clientcode", clientId)
                .get(baseURL, userIdSingle).then().contentType(ContentType.JSON).extract().body().jsonPath();
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        if(activeTargets.size()==1) {
            for (ActiveTarget target : activeTargets) {

                if(target.getInstanceId()==targetSingleInstance.getId()&&target.getClientId().equals(targetSingleInstance.getClientCode())&&
                        targetSingleInstance.getAchievementType().equals(target.getTargetType())&&targetSingleInstance.getLabel().equals(target.getName())
                        &&targetSingleInstance.getDefId()==target.getFactDefId()&&target.getUserId().equals(userVsAccessibleHierarchyKeys.getUserId())
                        &&target.getClientId().equals(userVsAccessibleHierarchyKeys.getClientId()))
                {
                    assertTrue(true);
                }

            }
        }
        else
        {
            fail("should return one record");
        }
    }
    // multiple instances self= false

    public void responseGetAllActiveTargetsTestSelfFalseResponseInstancesVerify() {

        JsonPath jsonPath = given().param("self", false)
                .param("clientcode", clientId)
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);

        if(activeTargets.size()==targetInstance.size()) {
            for (ActiveTarget target : activeTargets) {

                    TargetInstance instanceMatched = targetInstance.get(target.getInstanceId());
                    if (target.getInstanceId().equals(instanceMatched.getId()) &&
                            target.getMetricFactId().equals(instanceMatched.getMetricFactId()) &&
                            (target.getFactDefId() == instanceMatched.getDefId()) &&
                            (target.getClientId().equals(instanceMatched.getClientCode()))) {

                        assertTrue(true);

                    }

            }
        }
        else
            fail("should have 5 instances");
    }

    // multiple instances self= true

    public void responseVerificationOfMultipleInsatncesSelfTrue() {

        JsonPath jsonPath = given().param("self", true)
                .param("clientcode", clientId)
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        if(activeTargets.size()==targetInstance.size()) {
            for (ActiveTarget target : activeTargets) {

                    TargetInstance instanceMatched = targetInstance.get(target.getInstanceId());
                    if (target.getInstanceId().equals(instanceMatched.getId()) &&
                            target.getMetricFactId().equals(instanceMatched.getMetricFactId()) &&
                            (target.getFactDefId() == instanceMatched.getDefId()) &&
                            (target.getClientId().equals(instanceMatched.getClientCode()))) {
                        assertTrue(true);

                    }
                    else
                        assertFalse(false);
                }

        }
        else
            fail("should have 5 instances");
    }

    // multiple definitions self= true

    public void responseVerificationOfMultipleDefinitionsSelfTrue() {

        JsonPath jsonPath = given().param("self", true)
                .param("clientcode", clientId)
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        List<Integer> targetDefinitions=jsonPath.getList("targets.fact_definition_id");
        Set<Integer> targetDefinitionSet=new HashSet<Integer>(targetDefinitions);
        if(activeTargets.size()==targetInstance.size()&&targetDefinitionSet.size()==targetDefinitionHashMap.size()) {
            for (ActiveTarget target : activeTargets) {

                if (targetDefinitionHashMap.containsKey(target.getFactDefId())) {

                    TargetDefinition definitionMatched = targetDefinitionHashMap.get(target.getFactDefId());

                    if (target.getFactDefId() == (definitionMatched.getId()) &&
                            target.getMetricId().equals(definitionMatched.getFactId()) &&
                            (target.getClientId().equals(definitionMatched.getClientCode()))) {

                        assertTrue(true);

                    }
                }
            }
        }
        else
            fail("should have 5 instances and 2 target definitions");
    }

    // multiple definitions self= false

    public void responseVerificationOfMultipleDefinitionsSelfFalse() {

        JsonPath jsonPath = given().param("self", false)
                .param("clientcode", clientId)
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        List<Integer> targetDefinitions=jsonPath.getList("targets.fact_definition_id");
        Set<Integer> targetDefinitionSet=new HashSet<Integer>(targetDefinitions);
        if(activeTargets.size()==targetInstance.size()&&targetDefinitionSet.size()==targetDefinitionHashMap.size()) {
            for (ActiveTarget target : activeTargets) {

                    TargetDefinition definitionMatched = targetDefinitionHashMap.get(target.getFactDefId());
                    if (target.getFactDefId() == (definitionMatched.getId()) &&
                            target.getMetricId().equals(definitionMatched.getFactId()) &&
                            (target.getClientId().equals(definitionMatched.getClientCode()))) {
                        assertTrue(true);

                    }

            }
        }
        else
            fail("should have 5 instances and 2 target definitions");
    }

    // multiple instances self = true- uniqueness of the query

    public void responseGetAllActiveTargetsTestSelfTrueResponseInstancesVerifyTrue() {
        response=given().param("self",true)
                .param("clientcode",clientId)
                .get(baseURL,userId).then().contentType(ContentType.JSON).extract().response();
        List<Integer> intancesIds=response.path("targets.instance_id");
        Integer instanceFirst=intancesIds.get(0);
        intancesIds.remove(0);
        for(Integer instance:intancesIds)
        {
            if(targetInstance.get(instance)!=null) {
                assertThat(instanceFirst, not(instance));
            }
            else
                fail("instances are duplicated");
        }

    }
    // multiple instances self = false- uniqueness of the query

    public void responseGetAllActiveTargetsTestSelfTrueResponseInstancesVerifyFalse() {
        response=given().param("self",false)
                .param("clientcode",clientId)
                .get(baseURL,userId).then().contentType(ContentType.JSON).extract().response();
        List<Integer> instancesIds=response.path("targets.instance_id");
        Integer instanceFirst=instancesIds.get(0);
        instancesIds.remove(0);
        for(Integer instance:instancesIds)
        {
            assertThat(instanceFirst, not(instance));
        }

    }



// with date filter - test cases


    // API status OK

    @Test
    public void uriStatusforDateFilter() {
       given().param("self",false)
                .param("clientcode",clientId)
                .param("startdate",Date.valueOf(startDate))
                .param("enddate",Date.valueOf(endDate))
                .get(baseURL,userId).then().statusCode(200).log().all();

    }

    // multiple instances self= true with date filter

    public void responseVerificationDateFilterOfMultipleInsatncesSelfTrue() {

        JsonPath jsonPath = given().param("self", true)
                .param("clientcode", clientId)
                .param("startdate",Date.valueOf(startDate))
                .param("enddate",Date.valueOf(endDate))
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        int count=0;
        Date dateStart=Date.valueOf(startDate);
        Date dateEnd=Date.valueOf(endDate);
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        Long currentDate=System.currentTimeMillis();
        if(activeTargets.size()==targetInstance.size()) {
            for (ActiveTarget target : activeTargets) {

                TargetInstance instanceMatched = targetInstance.get(target.getInstanceId());

                if (target.getInstanceId().equals(instanceMatched.getId()) &&
                        target.getMetricFactId().equals(instanceMatched.getMetricFactId()) &&
                        (target.getFactDefId() == instanceMatched.getDefId()) &&
                        (target.getClientId().equals(instanceMatched.getClientCode())) && (dateEnd.getTime() - currentDate >= 10)) {
                    count++;

                }
                else
                    fail("one of the instance is not matched");

            }

            if(count==targetInstance.size())
                assertTrue(true);
            else
                fail("mismatch of records returned by API");
        }
        else
            fail("should have 5 instances");
    }

    // multiple instances self= false with date filter

    public void responseVerificationDateFilterOfMultipleInsatncesSelfFalse() {

        JsonPath jsonPath = given().param("self", false)
                .param("clientcode", clientId)
                .param("startdate",Date.valueOf(startDate))
                .param("enddate",Date.valueOf(endDate))
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        int count=0;
        Date dateStart=Date.valueOf(startDate);
        Date dateEnd=Date.valueOf(endDate);
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        Long currentDate=System.currentTimeMillis();
        if(activeTargets.size()==targetInstance.size()) {
            for (ActiveTarget target : activeTargets) {

                TargetInstance instanceMatched = targetInstance.get(target.getInstanceId());
                if (target.getInstanceId().equals(instanceMatched.getId()) &&
                        target.getMetricFactId().equals(instanceMatched.getMetricFactId()) &&
                        (target.getFactDefId() == instanceMatched.getDefId()) &&
                        (target.getClientId().equals(instanceMatched.getClientCode()))&&(dateEnd.getTime() - currentDate >= 10) ) {
                   count++;

                }
                else
                    fail("one of the instance is not matched");

            }
            if(count==targetInstance.size())
                assertTrue(true);
            else
                fail("mismatch of records returned by API");
        }
        else
            fail("should have 5 instances");
    }
    // multiple instances and target definition self = true

    public void responseDateFilterDefinitionsWhenSelfTrue() {

        JsonPath jsonPath = given().param("self", true)
                .param("clientcode", clientId)
                .param("startdate",Date.valueOf(startDate))
                .param("enddate",Date.valueOf(endDate))
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        Date dateStart=Date.valueOf(startDate);
        Date dateEnd=Date.valueOf(endDate);
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        List<Integer> targetDefinitions=jsonPath.getList("targets.fact_definition_id");
        Set<Integer> targetDefinitionSet=new HashSet<Integer>(targetDefinitions);
        Long currentDate=System.currentTimeMillis();
        if(activeTargets.size()==targetInstance.size()&&targetDefinitionSet.size()==targetDefinitionHashMap.size()) {
            for (ActiveTarget targets : activeTargets) {
                if (targetInstance.containsKey(targets.getInstanceId()) && targets.getClientId().equals(clientId) &&
                        ((targetDefinitionHashMap.containsKey(targets.getFactDefId())))
                        && (targets.getUserId().equals(userId)) && (dateEnd.getTime() - currentDate >= 10)) {
                    assertTrue(true);
                } else
                    fail("inactive target instances");
            }

        }
        else
            fail("should have 5 instances");
    }

    // multiple instances and target definition self = false

    public void responseDateFilterDefinitionsWhenSelfFalse() {

        JsonPath jsonPath = given().param("self", true)
                .param("clientcode", clientId)
                .param("startdate",Date.valueOf(startDate))
                .param("enddate",Date.valueOf(endDate))
                .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
        Date dateStart=Date.valueOf(startDate);
        Date dateEnd=Date.valueOf(endDate);
        List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
        List<Integer> targetDefinitions=jsonPath.getList("targets.fact_definition_id");
        Set<Integer> targetDefinitionSet=new HashSet<Integer>(targetDefinitions);
        Long currentDate=System.currentTimeMillis();
        if(activeTargets.size()==targetInstance.size()&&targetDefinitionHashMap.size()==targetDefinitionSet.size()) {
            for (ActiveTarget targets : activeTargets) {
                if (targetInstance.containsKey(targets.getInstanceId()) && targets.getClientId().equals(clientId) &&
                        ((targetDefinitionHashMap.containsKey(targets.getFactDefId())))
                        && (targets.getUserId().equals(userId)) && (dateEnd.getTime() - currentDate >= 10)) {
                    assertTrue(true);
                } else {
                    fail("inactive target instances");
                }
            }
        }
        else
            fail("should have 5 instances");

    }
    // multiple instances and target definition self = false - Detailed view

    public void responseDateFilterDefFilterDetailedViewInstancesWhenSelfTrue() {

        for (Map.Entry<Integer, TargetDefinition> entry : targetDefinitionHashMap.entrySet()) {

            JsonPath jsonPath = given().param("self", false)
                    .param("clientcode", clientId)
                    .param("startdate", Date.valueOf(startDate))
                    .param("enddate", Date.valueOf(endDate))
                    .param("targetdefinitionid", entry.getKey())
                    .param("metricfactid", metricfactid)
                    .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();
            Date dateStart=Date.valueOf(startDate);
            Date dateEnd=Date.valueOf(endDate);
            List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);
            Long currentDate=System.currentTimeMillis();
            System.out.println("date"+(dateEnd.getTime() - currentDate >= 10));

            for (ActiveTarget targets : activeTargets) {
                System.out.println("ins xyz"+targetInstance.containsKey(targets.getInstanceId())+".."+targets.getClientId().equals(clientId)+".."+targets.getUserId()+".."+userId);
                System.out.println("def"+(targetDefinitionHashMap.containsKey(targets.getFactDefId())));
                if (targetInstance.containsKey(targets.getInstanceId()) && targets.getClientId().equals(clientId) &&
                        ((targetDefinitionHashMap.containsKey(targets.getFactDefId()))) && (dateEnd.getTime() - currentDate >= 10)
                        ) {
                    assertTrue(true);
                }
                else
                    fail("showing invalid target definition");
            }

        }
    }

    // multiple instances and target definition self = false - Detailed view

    public void responseDefFilterDetailedViewInstancesWhenSelfFalse() {

        for (Map.Entry<Integer, TargetDefinition> entry : targetDefinitionHashMap.entrySet()) {
            System.out.println(entry.getKey() + "/" + entry.getValue());
            JsonPath jsonPath = given().param("self", false)
                    .param("clientcode", clientId)
                    .param("targetdefinitionid", entry.getKey())
                    .param("metricfactid", metricfactid)
                    .get(baseURL, userId).then().contentType(ContentType.JSON).extract().body().jsonPath();

            List<ActiveTarget> activeTargets = jsonPath.getList("targets", ActiveTarget.class);

            for (ActiveTarget targets : activeTargets) {
                if (targetInstance.containsKey(targets.getInstanceId()) && targets.getClientId().equals(clientId) &&
                        ((targetDefinitionHashMap.containsKey(targets.getFactDefId())))
                        ) {
                    assertTrue(true);
                }
                else
                    fail("showing invalid target definition");
            }

        }
    }





}
